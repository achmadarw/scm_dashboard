-- scm_dashboard.rkm definition

CREATE TABLE `rkm` (
    `id` varchar(100) NOT NULL DEFAULT 'uuid()',
    `sumur_code` varchar(100) DEFAULT NULL,
    `program` varchar(100) DEFAULT NULL,
    `plant_code` varchar(100) NOT NULL,
    `rkm_qty` int DEFAULT '0',
    `plan_date` varchar(100) DEFAULT NULL,
    `plan_tajak` varchar(100) DEFAULT NULL,
    `year` varchar(100) DEFAULT NULL,
    `quarter` varchar(100) DEFAULT NULL,
    `status_all` float DEFAULT '0',
    `status_area` float DEFAULT '0',
    `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_date` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- scm_dashboard.rkm_detail definition

CREATE TABLE `rkm_detail` (
    `id` varchar(100) NOT NULL DEFAULT 'uuid()',
    `rkm_id` varchar(100) NOT NULL,
    `kimap_code` varchar(100) NOT NULL,
    `unit` varchar(100) DEFAULT NULL,
    `quantity` int DEFAULT '0',
    `order_quantity` int DEFAULT '0',
    `stock_on_hand_quantity` int DEFAULT '0',
    `selisih` int DEFAULT '0',
    `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_date` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- scm_dashboard.soh_pep definition

CREATE TABLE `soh_pep` (
    `id` varchar(100) NOT NULL DEFAULT 'uuid()',
    `kimap_code` varchar(100) DEFAULT NULL,
    `storage_location` varchar(100) DEFAULT NULL,
    `plant_code` varchar(100) NOT NULL,
    `unrestricted` int DEFAULT '0',
    `value_unrestricted` double DEFAULT '0',
    `base_unit` varchar(100) DEFAULT NULL,
    `stock_in_transit` int DEFAULT '0',
    `transit_and_transfer` int DEFAULT '0',
    `in_quality_insp` int DEFAULT '0',
    `blocked` int DEFAULT '0',
    `batch` varchar(100) DEFAULT NULL,
    `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_date` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- scm_dashboard.soh_phe definition

CREATE TABLE `soh_phe` (
    `id` varchar(100) NOT NULL DEFAULT 'uuid()',
    `kimap_code` varchar(100) DEFAULT NULL,
    `storage_location` varchar(100) DEFAULT NULL,
    `desc_storage_location` varchar(100) DEFAULT NULL,
    `plant_code` varchar(100) NOT NULL,
    `unrestricted` int DEFAULT '0',
    `value_unrestricted` double DEFAULT '0',
    `base_unit` varchar(100) DEFAULT NULL,
    `material_type` varchar(100) DEFAULT NULL,
    `material_group` varchar(100) DEFAULT NULL,
    `spesial_stock` varchar(100) DEFAULT NULL,
    `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_date` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- scm_dashboard.`order` definition

CREATE TABLE `order` (
    `id` varchar(100) NOT NULL DEFAULT 'uuid()',
    `plant_code` varchar(100) NOT NULL,
    `outline_agreement` varchar(100) DEFAULT NULL,
    `purchasing_doc` varchar(100) DEFAULT NULL,
    `document_date` timestamp NOT NULL,
    `purchasing_doc_type` varchar(100) DEFAULT NULL,
    `acct_assignment_cat` varchar(100) DEFAULT NULL,
    `kimap_code` varchar(100) NOT NULL,
    `order_quantity` int NOT NULL,
    `used` int DEFAULT '0',
    `order_price_unit` varchar(100) DEFAULT NULL,
    `item` int DEFAULT NULL,
    `purchasing_group` varchar(100) DEFAULT NULL,
    `name_of_vendor` varchar(100) DEFAULT NULL,
    `currency` varchar(100) DEFAULT NULL,
    `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_date` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;