<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Rkm;

/**
 * RkmSearch represents the model behind the search form of `app\models\Rkm`.
 */
class RkmSearch extends Rkm
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sumur_code', 'program', 'plant_code', 'created_date', 'updated_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rkm::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'sumur_code', $this->sumur_code])
            ->andFilterWhere(['like', 'program', $this->program])
            ->andFilterWhere(['like', 'plant_code', $this->plant_code]);

        return $dataProvider;
    }
}
