<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\order;

/**
 * orderSearch represents the model behind the search form of `app\models\order`.
 */
class orderSearch extends order
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'plant_code', 'outline_agreement', 'purchasing_doc', 'document_date', 'purchasing_doc_type', 'acct_assignment_cat', 'kimap_code', 'order_price_unit', 'purchasing_group', 'name_of_vendor', 'currency', 'created_date', 'updated_date'], 'safe'],
            [['order_quantity', 'item'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'document_date' => $this->document_date,
            'order_quantity' => $this->order_quantity,
            'item' => $this->item,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'plant_code', $this->plant_code])
            ->andFilterWhere(['like', 'outline_agreement', $this->outline_agreement])
            ->andFilterWhere(['like', 'purchasing_doc', $this->purchasing_doc])
            ->andFilterWhere(['like', 'purchasing_doc_type', $this->purchasing_doc_type])
            ->andFilterWhere(['like', 'acct_assignment_cat', $this->acct_assignment_cat])
            ->andFilterWhere(['like', 'kimap_code', $this->kimap_code])
            ->andFilterWhere(['like', 'order_price_unit', $this->order_price_unit])
            ->andFilterWhere(['like', 'purchasing_group', $this->purchasing_group])
            ->andFilterWhere(['like', 'name_of_vendor', $this->name_of_vendor])
            ->andFilterWhere(['like', 'currency', $this->currency]);

        return $dataProvider;
    }
}
