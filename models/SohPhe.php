<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "soh_phe".
 *
 * @property string $id
 * @property string|null $kimap_code
 * @property string|null $storage_location
 * @property string|null $desc_storage_location
 * @property string $plant_code
 * @property int|null $unrestricted
 * @property float|null $value_unrestricted
 * @property string|null $base_unit
 * @property string|null $material_type
 * @property string|null $material_group
 * @property string|null $spesial_stock
 * @property string $created_date
 * @property string|null $updated_date
 */
class SohPhe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'soh_phe';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'plant_code'], 'required'],
            [['unrestricted'], 'integer'],
            [['value_unrestricted'], 'number'],
            [['created_date', 'updated_date'], 'safe'],
            [['id', 'kimap_code', 'storage_location', 'desc_storage_location', 'plant_code', 'base_unit', 'material_type', 'material_group', 'spesial_stock'], 'string', 'max' => 100],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kimap_code' => 'Kimap Code',
            'storage_location' => 'Storage Location',
            'desc_storage_location' => 'Desc Storage Location',
            'plant_code' => 'Plant Code',
            'unrestricted' => 'Unrestricted',
            'value_unrestricted' => 'Value Unrestricted',
            'base_unit' => 'Base Unit',
            'material_type' => 'Material Type',
            'material_group' => 'Material Group',
            'spesial_stock' => 'Spesial Stock',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }
}
