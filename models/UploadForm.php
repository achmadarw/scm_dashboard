<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

/**
 * UploadForm is the model behind the upload form.
 */
class UploadForm extends Model
{
    /**
     * @var UploadedFile file attribute
     */
    public $file;
    public $path;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['file'], 'file'],
        ];
    }

    public function upload() {
        if ($this->validate()) {
            $this->path = '../uploads/' . $this->file->baseName . '.' . $this->file->extension;
            $this->file->saveAs($this->path);
            return true;
        } else {
            return false;
        }
     }
}