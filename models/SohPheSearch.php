<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SohPhe;

/**
 * SohPheSearch represents the model behind the search form of `app\models\SohPhe`.
 */
class SohPheSearch extends SohPhe
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'kimap_code', 'storage_location', 'desc_storage_location', 'plant_code', 'base_unit', 'material_type', 'material_group', 'spesial_stock', 'created_date', 'updated_date'], 'safe'],
            [['unrestricted'], 'integer'],
            [['value_unrestricted'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SohPhe::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'unrestricted' => $this->unrestricted,
            'value_unrestricted' => $this->value_unrestricted,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'kimap_code', $this->kimap_code])
            ->andFilterWhere(['like', 'storage_location', $this->storage_location])
            ->andFilterWhere(['like', 'desc_storage_location', $this->desc_storage_location])
            ->andFilterWhere(['like', 'plant_code', $this->plant_code])
            ->andFilterWhere(['like', 'base_unit', $this->base_unit])
            ->andFilterWhere(['like', 'material_type', $this->material_type])
            ->andFilterWhere(['like', 'material_group', $this->material_group])
            ->andFilterWhere(['like', 'spesial_stock', $this->spesial_stock]);

        return $dataProvider;
    }
}
