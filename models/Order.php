<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property string $id
 * @property string $plant_code
 * @property string|null $outline_agreement
 * @property string|null $purchasing_doc
 * @property string $document_date
 * @property string|null $purchasing_doc_type
 * @property string|null $acct_assignment_cat
 * @property string $kimap_code
 * @property int $order_quantity
 * @property string|null $order_price_unit
 * @property int|null $item
 * @property string|null $purchasing_group
 * @property string|null $name_of_vendor
 * @property string|null $currency
 * @property string $created_date
 * @property string|null $updated_date
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'plant_code', 'document_date', 'kimap_code', 'order_quantity'], 'required'],
            [['document_date', 'created_date', 'updated_date'], 'safe'],
            [['order_quantity', 'item', 'used'], 'integer'],
            [['id', 'plant_code', 'outline_agreement', 'purchasing_doc', 'purchasing_doc_type', 'acct_assignment_cat', 'kimap_code', 'order_price_unit', 'purchasing_group', 'name_of_vendor', 'currency'], 'string', 'max' => 100],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'plant_code' => 'Plant Code',
            'outline_agreement' => 'Outline Agreement',
            'purchasing_doc' => 'Purchasing Doc',
            'document_date' => 'Document Date',
            'purchasing_doc_type' => 'Purchasing Doc Type',
            'acct_assignment_cat' => 'Acct Assignment Cat',
            'kimap_code' => 'Kimap Code',
            'order_quantity' => 'Order Quantity',
            'used' => 'Used',
            'order_price_unit' => 'Order Price Unit',
            'item' => 'Item',
            'purchasing_group' => 'Purchasing Group',
            'name_of_vendor' => 'Name Of Vendor',
            'currency' => 'Currency',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }
}
