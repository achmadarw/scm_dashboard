<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rkm".
 *
 * @property string $id
 * @property string|null $sumur_code
 * @property string|null $program
 * @property string $plant_code
 * @property string $created_date
 * @property string $plan_tajak
 * @property string|null $updated_date
 */
class Rkm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rkm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'plant_code'], 'required'],
            [['created_date', 'updated_date', 'plan_date','plan_tajak'], 'safe'],
            [['id', 'sumur_code', 'program', 'plant_code', 'year'], 'string', 'max' => 100],
            [['status_all', 'status_area'], 'number'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sumur_code' => 'Sumur Code',
            'program' => 'Program',
            'plant_code' => 'Plant',
            'plan_date' => 'Plan Date',
            'plan_tajak' => 'Plan Tajak',
            'rkm_qty' => 'RKM QTY',
            'status_all' => 'Status All',
            'status_area' => 'Status Area',
            'year' => 'Year',
            'quarter' => 'Quarter',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }
}
