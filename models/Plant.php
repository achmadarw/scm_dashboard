<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plant".
 *
 * @property int $id
 * @property string $plant_code
 * @property string $field
 * @property string $area
 * @property string $ap
 * @property string $created_date
 * @property string|null $updated_date
 */
class Plant extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plant';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plant_code', 'field', 'area', 'ap'], 'required'],
            [['created_date', 'updated_date'], 'safe'],
            [['plant_code', 'field', 'area', 'ap'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'plant_code' => 'Plant Code',
            'field' => 'Field',
            'area' => 'Area',
            'ap' => 'Ap',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }
}
