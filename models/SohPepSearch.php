<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SohPep;

/**
 * shoPepSearch represents the model behind the search form of `app\models\SohPep`.
 */
class SohPepSearch extends SohPep
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'kimap_code', 'storage_location', 'plant_code', 'base_unit', 'batch', 'created_date', 'updated_date'], 'safe'],
            [['unrestricted', 'stock_in_transit', 'transit_and_transfer', 'in_quality_insp', 'blocked'], 'integer'],
            [['value_unrestricted'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SohPep::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'unrestricted' => $this->unrestricted,
            'value_unrestricted' => $this->value_unrestricted,
            'stock_in_transit' => $this->stock_in_transit,
            'transit_and_transfer' => $this->transit_and_transfer,
            'in_quality_insp' => $this->in_quality_insp,
            'blocked' => $this->blocked,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'kimap_code', $this->kimap_code])
            ->andFilterWhere(['like', 'storage_location', $this->storage_location])
            ->andFilterWhere(['like', 'plant_code', $this->plant_code])
            ->andFilterWhere(['like', 'base_unit', $this->base_unit])
            ->andFilterWhere(['like', 'batch', $this->batch]);

        return $dataProvider;
    }
}
