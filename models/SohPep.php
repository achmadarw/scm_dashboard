<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "soh_pep".
 *
 * @property string $id
 * @property string|null $kimap_code
 * @property string|null $storage_location
 * @property string $plant_code
 * @property int|null $unrestricted
 * @property float|null $value_unrestricted
 * @property string|null $base_unit
 * @property int|null $stock_in_transit
 * @property int|null $transit_and_transfer
 * @property int|null $in_quality_insp
 * @property int|null $blocked
 * @property string|null $batch
 * @property string $created_date
 * @property string|null $updated_date
 */
class SohPep extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'soh_pep';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'plant_code'], 'required'],
            [['unrestricted', 'stock_in_transit', 'transit_and_transfer', 'in_quality_insp', 'blocked'], 'integer'],
            [['value_unrestricted'], 'number'],
            [['created_date', 'updated_date'], 'safe'],
            [['id', 'kimap_code', 'storage_location', 'plant_code', 'base_unit', 'batch'], 'string', 'max' => 100],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kimap_code' => 'Kimap Code',
            'storage_location' => 'Storage Location',
            'plant_code' => 'Plant Code',
            'unrestricted' => 'Unrestricted',
            'value_unrestricted' => 'Value Unrestricted',
            'base_unit' => 'Base Unit',
            'stock_in_transit' => 'Stock In Transit',
            'transit_and_transfer' => 'Transit And Transfer',
            'in_quality_insp' => 'In Quality Insp',
            'blocked' => 'Blocked',
            'batch' => 'Batch',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }
}
