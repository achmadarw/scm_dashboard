<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rkm_detail".
 *
 * @property string $id
 * @property string $rkm_id
 * @property string|null $kimap_code
 * @property int|null $quantity
 * @property string|null $unit
 * @property string $created_date
 * @property string|null $updated_date
 */
class RkmDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rkm_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'rkm_id'], 'required'],
            [['quantity','order_quantity','stock_on_hand_quantity'], 'integer'],
            [['created_date', 'updated_date'], 'safe'],
            [['id', 'rkm_id', 'kimap_code', 'unit'], 'string', 'max' => 100],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rkm_id' => 'Rkm ID',
            'kimap_code' => 'Kimap Code',
            'quantity' => 'Quantity',
            'order_quantity' => 'Order',
            'stock_on_hand_quantity' => 'Stock On Hand',
            'unit' => 'Unit',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }
}
