<?php

namespace app\controllers;

use app\models\SohPep;
use app\models\SohPepSearch;
use app\models\UploadForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ShoPepController implements the CRUD actions for SohPep model.
 */
class SohPepController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all SohPep models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SohPepSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SohPep model.
     * @param string $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SohPep model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $uploadModel = new UploadForm();
        $SohPep = new SohPep();

        if ($this->request->isPost) {
            $this->clearSohPep();
            $this->clearRkm();
            $this->clearRkmDetail();
            $uploadModel->file = UploadedFile::getInstance($uploadModel, 'file');
            if ($uploadModel->upload()) {
                // file is uploaded successfully
                $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($uploadModel->path);
                $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($uploadModel->path);
                try {
                    //$num=$objPHPExcel->getSheetCount();//get number of sheets
                    //$sheetnames=$objPHPExcel->getSheetNames();//get sheet names
                    $sheet = $objPHPExcel->getSheet(0);
                    $highestRow = $sheet->getHighestDataRow();
                    $highestColumn = $sheet->getHighestColumn();
                    //$row is start 2 because first row assigned for heading.
                    //echo "Highest Data Row $highestRow <br>";
                    for($row=2; $row<=$highestRow; ++$row) {
                        //$row=2;
                        $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);
                        $emptyRow = true;
                        $countData = count($rowData);
                    
                        //save to order table.
                        $SohPepTable = new SohPep();
                        $SohPepTable->loadDefaultValues();
                        $SohPepTable->id = $this->uuid();
                        $SohPepTable->plant_code = $rowData[0][0];
                        $SohPepTable->kimap_code = $rowData[0][1];
                        $SohPepTable->storage_location = $rowData[0][3];
                        $SohPepTable->unrestricted = $rowData[0][4];
                        $SohPepTable->base_unit = $rowData[0][5];
                        $SohPepTable->stock_in_transit = $rowData[0][6];
                        $SohPepTable->transit_and_transfer = $rowData[0][7];
                        $SohPepTable->in_quality_insp = $rowData[0][8];
                        $SohPepTable->blocked = $rowData[0][9];
                        $SohPepTable->value_unrestricted = $rowData[0][10];
                        $SohPepTable->batch = $rowData[0][11];
                        $SohPepTable->created_date = new Expression('NOW()');
                        
                        if ($SohPepTable->plant_code != null) {
                            if (!$SohPepTable->save()) {
                                //echo "Save order row .$row. failed $countData <br>";
                            } else {
                                //echo "Save order row .$row. Success $countData <br>";
                            }
                        }
                    }
                    //echo "Save order success";
                    unlink($uploadModel->path);
                    $searchModel = new SohPepSearch();
                    $dataProvider = $searchModel->search($this->request->queryParams);

                    return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]); 

                    return; 
                } catch (Exception $e){
                    //$transaction->rollBack();
                    echo "Save order failed " . $uploadModel->path;
                    //unlink($uploadModel->path);
                    return; 
                }
                echo "Save order failed " . $uploadModel->path;
                unlink($uploadModel->path);
                return;
             }
        } else {
            $SohPep->loadDefaultValues();
        }

        return $this->render('create', ['uploadModel' => $uploadModel]);
    }

    public function clearSohPep() {
        return \Yii::$app->db
        ->createCommand('DELETE FROM `soh_pep`')
        ->execute();
    }

    public function clearRkm() {
        return \Yii::$app->db
        ->createCommand('UPDATE rkm SET plan_date=null, plan_tajak=null, quarter = null, status_all=0, status_area=0')
        ->execute();
    }

    public function clearRkmDetail() {
        return \Yii::$app->db
        ->createCommand('UPDATE rkm_detail SET order_quantity=0, stock_on_hand_quantity=0, selisih=0')
        ->execute();
    }

    private function uuid($prefix = '')
	{
		$chars = md5(uniqid(mt_rand(), true));
		$uuid  = substr($chars,0,8) . '-';
		$uuid .= substr($chars,8,4) . '-';
		$uuid .= substr($chars,12,4) . '-';
		$uuid .= substr($chars,16,4) . '-';
		$uuid .= substr($chars,20,12);
		return $prefix . $uuid;
	}

    /**
     * Updates an existing SohPep model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SohPep model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SohPep model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id ID
     * @return SohPep the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SohPep::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
