<?php

namespace app\controllers;

use app\models\order;
use app\models\orderSearch;
use app\models\UploadForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\db\Expression;
use yii\base\Application;

/**
 * OrderController implements the CRUD actions for order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all order models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new orderSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single order model.
     * @param string $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $uploadModel = new UploadForm();
        $order = new order();

        if ($this->request->isPost) {
            $this->clearOrder();
            $this->clearRkm();
            $this->clearRkmDetail();
            $uploadModel->file = UploadedFile::getInstance($uploadModel, 'file');
            if ($uploadModel->upload()) {
                // file is uploaded successfully
                $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($uploadModel->path);
                $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($uploadModel->path);
                try {
                    //$num=$objPHPExcel->getSheetCount();//get number of sheets
                    //$sheetnames=$objPHPExcel->getSheetNames();//get sheet names
                    $sheet = $objPHPExcel->getSheet(0);
                    $highestRow = $sheet->getHighestDataRow();
                    $highestColumn = $sheet->getHighestColumn();
                    //$row is start 2 because first row assigned for heading.
                    //echo "Highest Data Row $highestRow <br>";
                    for($row=2; $row<=$highestRow; ++$row) {
                        //$row=2;
                        $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);
                        $emptyRow = true;
                        $countData = count($rowData);
                        //echo "row : $row <br>";
                    
                        //save to order table.
                        $orderTable = new order();
                        $orderTable->loadDefaultValues();
                        $orderTable->id = $this->uuid();
                        $orderTable->plant_code = $rowData[0][0];
                        $orderTable->outline_agreement = $rowData[0][1];
                        $orderTable->purchasing_doc = $rowData[0][1];
                        $orderTable->document_date = $this->mapDate($rowData[0][3])->format('Y-m-d H-i-s');
                        $orderTable->purchasing_doc_type = $rowData[0][4];
                        $orderTable->acct_assignment_cat = $rowData[0][5];
                        $orderTable->kimap_code = $rowData[0][6];
                        $orderTable->order_quantity = $rowData[0][8];
                        $orderTable->order_price_unit = $rowData[0][9];
                        $orderTable->item = $rowData[0][10];
                        $orderTable->purchasing_group = $rowData[0][11];
                        $orderTable->name_of_vendor = $rowData[0][12];
                        $orderTable->currency = $rowData[0][13];
                        $orderTable->created_date = new Expression('NOW()');
                        
                        if (!$orderTable->save()) {
                            echo "Save order row .$row. failed <br>";
                        }
                        //$docDate = gmdate("d-m-Y", $UNIX_DATE);
                        //echo "Save order failed $orderTable->document_date <br>";
                    }
                    //echo "Save order success";
                    unlink($uploadModel->path);
                    $searchModel = new orderSearch();
                    $dataProvider = $searchModel->search($this->request->queryParams);

                    return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]); 

                    //return; 
                } catch (Exception $e){
                    //$transaction->rollBack();
                    echo "Save order failed " . $uploadModel->path;
                    unlink($uploadModel->path);
                    return; 
                }
                echo "Save order failed " . $uploadModel->path;
                unlink($uploadModel->path);
                return;
             }
        } else {
            $order->loadDefaultValues();
        }

        return $this->render('create', ['uploadModel' => $uploadModel]);
    }

    private function mapDate($data) {
        $UNIX_DATE = ($data - 25569) * 86400;
        $dateTime = new \DateTime(); //this returns the current date time
        return $dateTime->setTimestamp($UNIX_DATE);
    }

    private function uuid($prefix = '')
	{
		$chars = md5(uniqid(mt_rand(), true));
		$uuid  = substr($chars,0,8) . '-';
		$uuid .= substr($chars,8,4) . '-';
		$uuid .= substr($chars,12,4) . '-';
		$uuid .= substr($chars,16,4) . '-';
		$uuid .= substr($chars,20,12);
		return $prefix . $uuid;
	}

    public function actionUpload()
    {
        $model = new order();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function clearOrder() {
        return \Yii::$app->db
        ->createCommand('DELETE FROM `order`')
        ->execute();
    }

    public function clearRkm() {
        return \Yii::$app->db
        ->createCommand('UPDATE rkm SET plan_date=null, plan_tajak=null, quarter = null, status_all=0, status_area=0')
        ->execute();
    }

    public function clearRkmDetail() {
        return \Yii::$app->db
        ->createCommand('UPDATE rkm_detail SET order_quantity=0, stock_on_hand_quantity=0, selisih=0')
        ->execute();
    }

    /**
     * Updates an existing order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id ID
     * @return order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = order::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
