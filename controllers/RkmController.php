<?php

namespace app\controllers;

use app\models\Rkm;
use app\models\RkmDetail;
use app\models\RkmDetailSearch;
use app\models\RkmSearch;
use app\models\UploadForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\db\Command;
use DateTime;

/**
 * RkmController implements the CRUD actions for Rkm model.
 */
class RkmController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Rkm models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new RkmSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Rkm model.
     * @param string $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchModel = new RkmDetailSearch();
        $dataProvider = $searchModel->search($this->request->queryParams, $id);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Rkm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $uploadModel = new UploadForm();
        $Rkm = new Rkm();

        if ($this->request->isPost) {
            $this->clearRkm();
            $this->clearRkmDetail();
            $uploadModel->file = UploadedFile::getInstance($uploadModel, 'file');
            if ($uploadModel->upload()) {
                // file is uploaded successfully
                $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($uploadModel->path);
                $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($uploadModel->path);
                try {
                    $num=$objPHPExcel->getSheetCount();//get number of sheets
                    $sheetnames=$objPHPExcel->getSheetNames();//get sheet names
                    for($i = 0 ; $i < $num ; $i++) {
                        //$sheetname = $sheetnames($i);
                        $sheet = $objPHPExcel->getSheet($i);
                        $highestRow = $sheet->getHighestDataRow();
                        $highestColumn = $sheet->getHighestColumn();
                        //$row is start 2 because first row assigned for heading.
                        //echo "Highest Data Row $highestRow <br>";

                        $rowData4 = $sheet->rangeToArray('B4:'.$highestColumn.'4',NULL,TRUE,FALSE);
                        $rowData5 = $sheet->rangeToArray('B5:'.$highestColumn.'5',NULL,TRUE,FALSE);
                        $rowData6 = $sheet->rangeToArray('B6:'.$highestColumn.'6',NULL,TRUE,FALSE);

                        $RkmTable = new Rkm();
                        $RkmTable->loadDefaultValues();
                        $RkmTable->id = $this->uuid();  
                        $RkmTable->sumur_code = $rowData4[0][1]; 
                        $RkmTable->program = $rowData5[0][1];  
                        $RkmTable->plant_code = $rowData6[0][1];  
                        if ($RkmTable->sumur_code != null) {
                            if (!$RkmTable->save()) {
                                //echo "Save order row .$row. failed $countData <br>";
                            } else {
                                //echo "Save order row .$row. Success $countData <br>";
                            }
                        }

                        $rkmQty = 0;

                        for($row=10; $row<=$highestRow; ++$row) {
                            $rowRkmDetail = $sheet->rangeToArray('B'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);

                            $RkmDetailTable = new RkmDetail();
                            $RkmDetailTable->loadDefaultValues();
                            $RkmDetailTable->id = $this->uuid();  
                            $RkmDetailTable->rkm_id = $RkmTable->id; 
                            if($rowRkmDetail[0][2] != null) {
                                $RkmDetailTable->kimap_code = $rowRkmDetail[0][2]; 
                                $RkmDetailTable->quantity = $rowRkmDetail[0][3];
                                $RkmDetailTable->unit = $rowRkmDetail[0][4];
                                $rkmQty = $rkmQty + $rowRkmDetail[0][3];

                                if (!$RkmDetailTable->save()) {
                                    //echo "Save order row .$row. failed $countData <br>";
                                } else {
                                    //echo "Save order row .$row. Success $countData <br>";
                                }
                            }
                        }
                        $RkmTable = Rkm::findOne($RkmTable->id);
                        $RkmTable->rkm_qty = $rkmQty;
                        if ($RkmTable->update() !== false) {
                            // update successful
                            echo "update rkm successful";
                        } else {
                            // update failed
                            echo "update rkm failed";
                        }
                    }
                    
                    //echo "Save order success";
                    unlink($uploadModel->path);
                    $searchModel = new RkmSearch();
                    $dataProvider = $searchModel->search($this->request->queryParams);

                    return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]); 
                } catch (Exception $e){
                    //$transaction->rollBack();
                    echo "Save order failed " . $uploadModel->path;
                    //unlink($uploadModel->path);
                    return; 
                }
                echo "Save order failed " . $uploadModel->path;
                unlink($uploadModel->path);
                return;
             }
        } else {
            $Rkm->loadDefaultValues();
        }

        return $this->render('create', ['uploadModel' => $uploadModel]);
    }

    private function uuid($prefix = '')
	{
		$chars = md5(uniqid(mt_rand(), true));
		$uuid  = substr($chars,0,8) . '-';
		$uuid .= substr($chars,8,4) . '-';
		$uuid .= substr($chars,12,4) . '-';
		$uuid .= substr($chars,16,4) . '-';
		$uuid .= substr($chars,20,12);
		return $prefix . $uuid;
	}

    /**
     * Updates an existing Rkm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            $dateTime = new DateTime($model->plan_date);
            $weekNumber = $dateTime->format("W");
            $year = $dateTime->format("Y");
            $strMonth = $dateTime->format("M");
            $monthNumber = date("m", strtotime($strMonth));
            $quarter = 'Q'.ceil($monthNumber/3);
            $plantTajak = $year.' '.$strMonth .' W'.$weekNumber;

            //$statusArea = $this->getStatusArea($model->sumur_code, $model->plan_date);
            //$statusAll = $this->getStatusAll($model->sumur_code, $model->plan_date);
            $simulationResult = $this->simulation($model->sumur_code, $model->plan_date);
            $model->plan_tajak = $plantTajak;
            $model->quarter = $quarter;
            $model->year = $year;
            //$model->status_area = $statusArea['kesiapan_area'];
            //$model->status_all = $statusAll['kesiapan_all'];

            $totalKimap = 0;
            $selisihOrderQuantity = 0;
            $selisihAll = 0;
            foreach ($simulationResult as $value) {
                $totalKimap = $totalKimap + 1;
                if ($value['selisih_order_quantity'] < 0) {
                    $selisihOrderQuantity = $selisihOrderQuantity + 1;
                }
                if ($value['total_selisih_all'] < 0) {
                    $selisihAll = $selisihAll + 1;
                }
                $this->updateRkmDetail($value['rkm_detail_id'], $value['order_quantity'], $value['pep_quantity'] + $value['phe_quantity']);
                if ($value['order_id'] != null) {
                    // $usedUpdate = 0;
                    // if ($value['rkm_quantity'] > $value['order_quantity']) {
                    //     $usedUpdate = $value['order_quantity'];
                    // } else {
                    //     $usedUpdate = $value['rkm_quantity'];
                    // }
                    $rkmQty = $value['rkm_quantity'];
                    $orders = $this->findOrders($model->plan_date, $model->plant_code, $value['kimap_code']);
                    $this->updateOrder($orders, $rkmQty, $value['order_quantity']);
                }
            }
            $statusArea = (($totalKimap - $selisihOrderQuantity)/$totalKimap)*100;
            $statusAll = (($totalKimap - $selisihAll)/$totalKimap)*100;
            $model->status_area = $statusArea;
            $model->status_all = $statusAll;
            //echo $statusArea."<br>";
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                echo 'update failed ';
                return;
            }
            //$res = $this->getStatus('KTT-X11', '2023-01-21');
           // $id= $res['kesiapan_area'];
            //echo $model->plan_date;
            //echo '<pre>'; print_r($simulationResult); echo '</pre>';
            return;
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function findOrders($planTajak, $plantCode, $kimapCode) {
        return \Yii::$app->db
        ->createCommand('select o.id, o.plant_code, o.kimap_code, o.order_quantity, o.used from `order` o where o.document_date <= :planTajak and o.plant_code = :plantCode and kimap_code = :kimapCode')
        ->bindValue(':planTajak', $planTajak)
        ->bindValue(':plantCode', $plantCode)
        ->bindValue(':kimapCode', $kimapCode)
        ->queryAll();
    }

    public function updateRkmDetail($id, $orderQuantity, $stockQuantity) {
        return \Yii::$app->db
        ->createCommand('UPDATE rkm_detail SET order_quantity=:orderQuantity, stock_on_hand_quantity=:stockQuantity WHERE id=:id')
        ->bindValue(':id', $id)
        ->bindValue(':orderQuantity', $orderQuantity)
        ->bindValue(':stockQuantity', $stockQuantity)
        ->execute();
    }

    public function updateOrder($orders, $rkmQty, $orderQuantity) {
        foreach ($orders as $value) {
            if ($rkmQty != 0) {
                $usedUpdate = $rkmQty + $value['used'];
                if ($usedUpdate > $value['order_quantity']) {
                    $usedUpdate = $value['order_quantity'];
                    $rkmQty = $rkmQty - ($value['order_quantity'] - $value['used']);
                } else {
                    $rkmQty = 0;
                }
                \Yii::$app->db
                    ->createCommand('UPDATE `order` SET used=:used WHERE id=:id')
                    ->bindValue(':id', $value['id'])
                    ->bindValue(':used', $usedUpdate)
                    ->execute();
            }
        }
    }

    public function clearOrder($orders, $quantity) {
        foreach ($orders as $value) {
            if ($quantity > 0) {
                if ($value['used'] < $quantity) {
                    $quantity = $quantity - $value['used'];
                    $used = 0;
                    \Yii::$app->db
                        ->createCommand('UPDATE `order` SET used=:used WHERE id=:id')
                        ->bindValue(':id', $value['id'])
                        ->bindValue(':used', $used)
                        ->execute();
                } else {
                    $used =  $value['used'] - $quantity;
                    $quantity = 0;
                    \Yii::$app->db
                        ->createCommand('UPDATE `order` SET used=:used WHERE id=:id')
                        ->bindValue(':id', $value['id'])
                        ->bindValue(':used', $used)
                        ->execute();
                }
            }
        }
    }

    public function simulation($sumurCode, $planTajak) {
        return \Yii::$app->db->createCommand('
        select
            rd.id as rkm_detail_id,
            p.ap,
            p.field,
            p.area,
            p.plant_code,
            odr.id as order_id,
            r.sumur_code, 
            rd.kimap_code, 
            rd.quantity as rkm_quantity, 
            (case when odr.order_quantity is null then 0 else odr.order_quantity end) as order_quantity,
            ((case when odr.order_quantity is null then 0 else odr.order_quantity end) - rd.quantity) as selisih_order_quantity,
            (case when pep.pep_quantity is null then 0 else pep.pep_quantity end) as pep_quantity,
            (case when phe.phe_quantity is null then 0 else phe.phe_quantity end) as phe_quantity,
            (((case when odr.order_quantity is null then 0 else odr.order_quantity end) + (case when pep.pep_quantity is null then 0 else pep.pep_quantity end) + (case when phe.phe_quantity is null then 0 else phe.phe_quantity end))-rd.quantity) as total_selisih_all
        from rkm_detail rd 
        inner join rkm r on r.id = rd.rkm_id
        inner join plant p on p.plant_code = r.plant_code
        left join (select o.id, o.plant_code, o.kimap_code, sum(o.order_quantity-o.used) as order_quantity from `order` o where o.document_date <= :planTajak group by o.kimap_code, o.plant_code) odr on odr.plant_code = r.plant_code and odr.kimap_code = rd.kimap_code and odr.plant_code = p.plant_code
        left join (select plant_code, kimap_code, sum(unrestricted) as pep_quantity from soh_pep group by kimap_code, plant_code) pep on pep.plant_code = r.plant_code and pep.kimap_code = rd.kimap_code and p.ap = "PEP"
            left join (select plant_code, kimap_code, sum(unrestricted) as phe_quantity from soh_phe group by kimap_code, plant_code) phe on pep.plant_code = r.plant_code and pep.kimap_code = rd.kimap_code and p.ap = "PHE"
        where r.sumur_code = :sumurCode
        ')->bindValue(':sumurCode', $sumurCode)->bindValue(':planTajak', $planTajak)->queryAll();
    }

    public function getStatusArea($sumurCode, $planTajak) {
        return \Yii::$app->db->createCommand('
            select 
            a.ap,
            a.plant_code,
            a.sumur_code,
            count(a.sumur_code) as total_item,
            count(if(a.selisih_order_quantity < 0, a.selisih_order_quantity, null)) as selisih,
            ((count(a.sumur_code) - count(if(a.selisih_order_quantity < 0, a.selisih_order_quantity, null)))/count(a.sumur_code)) as kesiapan_area
            from 
                    (select
                        p.ap,
                        p.field,
                        p.area,
                        p.plant_code,
                        odr.plant_code as order_plant_code,
                        r.sumur_code, 
                        rd.kimap_code, 
                        rd.quantity as rkm_quantity, 
                        (case when odr.order_quantity is null then 0 else odr.order_quantity end) as order_quantity,
                        ((case when odr.order_quantity is null then 0 else odr.order_quantity end) - rd.quantity) as selisih_order_quantity
                    from rkm_detail rd 
                    inner join rkm r on r.id = rd.rkm_id
                    inner join plant p on p.plant_code = r.plant_code
                    left join (select o.plant_code, o.kimap_code, sum(o.order_quantity) as order_quantity from `order` o where o.document_date <= :planTajak group by o.kimap_code, o.plant_code) odr on odr.plant_code = r.plant_code and odr.kimap_code = rd.kimap_code and odr.plant_code = p.plant_code) a
            where a.sumur_code = :sumurCode group by a.sumur_code
        ')->bindValue(':sumurCode', $sumurCode)->bindValue(':planTajak', $planTajak)->queryOne();
    }

    public function getStatusAll($sumurCode, $planTajak) {
        return \Yii::$app->db->createCommand('
            select 
            a.ap,
            a.plant_code,
            a.sumur_code,
            count(a.sumur_code) as total_item,
            count(if(a.selisih_order_quantity < 0, a.selisih_order_quantity, null)) as selisih,
            ((count(a.sumur_code) - count(if(a.selisih_order_quantity < 0, a.selisih_order_quantity, null)))/count(a.sumur_code)) as kesiapan_area,
            count(if(a.total_selisih_all < 0, a.total_selisih_all, null)) as selisih_ap,
            ((count(a.sumur_code) - count(if(a.total_selisih_all < 0, a.total_selisih_all, null)))/count(a.sumur_code)) as kesiapan_all
            from 
                    (select
                        p.ap,
                        p.field,
                        p.area,
                        p.plant_code,
                        odr.plant_code as order_plant_code,
                        r.sumur_code, 
                        rd.kimap_code, 
                        rd.quantity as rkm_quantity, 
                        (case when odr.order_quantity is null then 0 else odr.order_quantity end) as order_quantity,
                        ((case when odr.order_quantity is null then 0 else odr.order_quantity end) - rd.quantity) as selisih_order_quantity,
                        (((case when odr.order_quantity is null then 0 else odr.order_quantity end) + (case when pep.pep_quantity is null then 0 else pep.pep_quantity end) + (case when phe.phe_quantity is null then 0 else phe.phe_quantity end))-rd.quantity) as total_selisih_all
                    from rkm_detail rd 
                    inner join rkm r on r.id = rd.rkm_id
                    inner join plant p on p.plant_code = r.plant_code
                    left join (select o.plant_code, o.kimap_code, sum(o.order_quantity) as order_quantity from `order` o where o.document_date <= :planTajak group by o.kimap_code, o.plant_code) odr on odr.plant_code = r.plant_code and odr.kimap_code = rd.kimap_code and odr.plant_code = p.plant_code
                    left join (select plant_code, kimap_code, sum(unrestricted) as pep_quantity from soh_pep group by kimap_code, plant_code) pep on pep.plant_code = r.plant_code and pep.kimap_code = rd.kimap_code and p.ap = "PEP"
		            left join (select plant_code, kimap_code, sum(unrestricted) as phe_quantity from soh_phe group by kimap_code, plant_code) phe on pep.plant_code = r.plant_code and pep.kimap_code = rd.kimap_code and p.ap = "PHE") a
            where a.sumur_code = :sumurCode group by a.sumur_code
        ')->bindValue(':sumurCode', $sumurCode)->bindValue(':planTajak', $planTajak)->queryOne();
    }

    /**
     * Deletes an existing Rkm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $rkmModel = $this->findModel($id);
        $rkmDetailModel = $this->getRkmDetailByRkmId($id);
        // echo '<pre>'; print_r($rkmDetailModel); echo '</pre>';
        foreach ($rkmDetailModel as $value) {
            $orders = $this->findOrders($rkmModel->plan_date, $rkmModel->plant_code, $value['kimap_code']);
            
            if ($orders != null) {
                // echo '<pre>'; print_r($orders); echo '</pre>';
                $this->clearOrder($orders, $value['quantity']);
            }
            
        }
        
        // return;
        $this->clearRkmById($id);
        $this->clearRkmDetailById($id);
        return $this->redirect(['index']);
    }
    
    public function getRkmDetailByRkmId($id) {
        return \Yii::$app->db
        ->createCommand('select * from rkm_detail rd where rkm_id = :rkmId')
        ->bindValue(':rkmId', $id)
        ->queryAll();
    }

    public function clearRkmById($id) {
        return \Yii::$app->db
        ->createCommand('UPDATE rkm SET plan_date=null, plan_tajak=null, year = null, quarter = null, status_all=0, status_area=0 WHERE id=:rkmId')
        ->bindValue(':rkmId', $id)->execute();
    }

    public function clearRkmDetailById($rkmId) {
        return \Yii::$app->db
        ->createCommand('UPDATE rkm_detail SET order_quantity=0, stock_on_hand_quantity=0, selisih=0 WHERE rkm_id=:rkmId')
        ->bindValue(':rkmId', $rkmId)
        ->execute();
    }

    public function clearRkm() {
        return \Yii::$app->db
        ->createCommand('DELETE FROM rkm')
        ->execute();
    }

    public function clearRkmDetail() {
        return \Yii::$app->db
        ->createCommand('DELETE FROM rkm_detail')
        ->execute();
    }

    /**
     * Finds the Rkm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id ID
     * @return Rkm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rkm::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findRkmDetailModel($id)
    {
        if (($model = RkmDetail::find(['rkm_id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
