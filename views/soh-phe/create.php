<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\SohPhe $model */

$this->title = 'Create Soh Phe';
$this->params['breadcrumbs'][] = ['label' => 'Soh Phes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="soh-phe-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
        <?= $form->field($uploadModel, 'file')->fileInput() ?>
        <div class="form-group">
            <?= Html::submitButton('Import', ['class' => 'btn btn-success']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>
