<?php

use app\models\SohPhe;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\SohPheSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Soh Phes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="soh-phe-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Soh Phe', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'kimap_code',
            'storage_location',
            'desc_storage_location',
            'plant_code',
            //'unrestricted',
            //'value_unrestricted',
            //'base_unit',
            //'material_type',
            //'material_group',
            //'spesial_stock',
            //'created_date',
            //'updated_date',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, SohPhe $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
