<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\SohPhe $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="soh-phe-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kimap_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'storage_location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'desc_storage_location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'plant_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unrestricted')->textInput() ?>

    <?= $form->field($model, 'value_unrestricted')->textInput() ?>

    <?= $form->field($model, 'base_unit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'material_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'material_group')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'spesial_stock')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <?= $form->field($model, 'updated_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
