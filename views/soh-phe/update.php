<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\SohPhe $model */

$this->title = 'Update Soh Phe: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Soh Phes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="soh-phe-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
