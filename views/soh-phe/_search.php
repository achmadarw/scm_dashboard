<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\SohPheSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="soh-phe-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'kimap_code') ?>

    <?= $form->field($model, 'storage_location') ?>

    <?= $form->field($model, 'desc_storage_location') ?>

    <?= $form->field($model, 'plant_code') ?>

    <?php // echo $form->field($model, 'unrestricted') ?>

    <?php // echo $form->field($model, 'value_unrestricted') ?>

    <?php // echo $form->field($model, 'base_unit') ?>

    <?php // echo $form->field($model, 'material_type') ?>

    <?php // echo $form->field($model, 'material_group') ?>

    <?php // echo $form->field($model, 'spesial_stock') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'updated_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
