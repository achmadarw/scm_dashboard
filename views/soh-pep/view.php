<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\SohPep $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Soh Peps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="soh-pep-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'kimap_code',
            'storage_location',
            'plant_code',
            'unrestricted',
            'value_unrestricted',
            'base_unit',
            'stock_in_transit',
            'transit_and_transfer',
            'in_quality_insp',
            'blocked',
            'batch',
            'created_date',
            'updated_date',
        ],
    ]) ?>

</div>
