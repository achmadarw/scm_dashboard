<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\SohPep $model */

$this->title = 'Create Soh Pep';
$this->params['breadcrumbs'][] = ['label' => 'Soh Peps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="soh-pep-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
        <?= $form->field($uploadModel, 'file')->fileInput() ?>
        <div class="form-group">
            <?= Html::submitButton('Import', ['class' => 'btn btn-success']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
