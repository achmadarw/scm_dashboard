<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\SohPep $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="soh-pep-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kimap_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'storage_location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'plant_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unrestricted')->textInput() ?>

    <?= $form->field($model, 'value_unrestricted')->textInput() ?>

    <?= $form->field($model, 'base_unit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'stock_in_transit')->textInput() ?>

    <?= $form->field($model, 'transit_and_transfer')->textInput() ?>

    <?= $form->field($model, 'in_quality_insp')->textInput() ?>

    <?= $form->field($model, 'blocked')->textInput() ?>

    <?= $form->field($model, 'batch')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <?= $form->field($model, 'updated_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
