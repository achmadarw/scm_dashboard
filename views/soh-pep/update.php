<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\SohPep $model */

$this->title = 'Update Soh Pep: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Soh Peps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="soh-pep-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
