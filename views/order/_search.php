<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\orderSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'plant_code') ?>

    <?= $form->field($model, 'outline_agreement') ?>

    <?= $form->field($model, 'purchasing_doc') ?>

    <?= $form->field($model, 'document_date') ?>

    <?php // echo $form->field($model, 'purchasing_doc_type') ?>

    <?php // echo $form->field($model, 'acct_assignment_cat') ?>

    <?php // echo $form->field($model, 'kimap_code') ?>

    <?php // echo $form->field($model, 'order_quantity') ?>

    <?php // echo $form->field($model, 'order_price_unit') ?>

    <?php // echo $form->field($model, 'item') ?>

    <?php // echo $form->field($model, 'purchasing_group') ?>

    <?php // echo $form->field($model, 'name_of_vendor') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'updated_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
