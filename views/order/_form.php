<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\order $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'plant_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'outline_agreement')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'purchasing_doc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'document_date')->textInput() ?>

    <?= $form->field($model, 'purchasing_doc_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'acct_assignment_cat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kimap_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order_quantity')->textInput() ?>

    <?= $form->field($model, 'order_price_unit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'item')->textInput() ?>

    <?= $form->field($model, 'purchasing_group')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_of_vendor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'currency')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <?= $form->field($model, 'updated_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
