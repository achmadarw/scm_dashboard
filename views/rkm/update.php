<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Rkm $model */

$this->title = 'Update Rkm: ' . $model->sumur_code;
$this->params['breadcrumbs'][] = ['label' => 'Rkms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rkm-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
