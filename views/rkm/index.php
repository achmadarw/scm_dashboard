<?php

use app\models\Rkm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\RkmSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Simulasi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rkm-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Rkm', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'sumur_code',
            'plan_tajak',
            [
                'attribute' => 'quarter',
                'value' => function ($model) {
                    return $model->year . ' ' . $model->quarter;
                }
            ],
            'rkm_qty',
            [
                'attribute' => 'status_all',
                'value' => function ($model) {
                    return $model->status_all . '%';
                }
            ],
            [
                'attribute' => 'status_area',
                'value' => function ($model) {
                    return $model->status_area . '%';
                }
            ],
            //'updated_date',
            [
                'class' => ActionColumn::className(),
                'template'=>'{view} {update} {delete}',
                'urlCreator' => function ($action, Rkm $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
