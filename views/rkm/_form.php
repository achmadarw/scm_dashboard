<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Rkm $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="rkm-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'plan_date')->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
